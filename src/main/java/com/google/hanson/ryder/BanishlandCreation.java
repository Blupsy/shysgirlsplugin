package com.google.hanson.ryder;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.block.Block;

public class BanishlandCreation {
	
	private static int _structureWidth = 20;
	private static int _structureHeight = 20;
	private static int _structureDepth = 20;
	
	public static void CreateCubeEnclosure(Location location) {
		int xBlockLocation = location.getBlockX();
		int yBlockLocation = location.getBlockY();
		int zBlockLocation = location.getBlockZ();
		
		int xBlockLocationEnd = xBlockLocation + _structureWidth;
		int yBlockLocationEnd = yBlockLocation + _structureHeight;
		int zBlockLocationEnd = zBlockLocation + _structureDepth;

		
		World world = location.getWorld();

		for (int x = xBlockLocation; x <= xBlockLocationEnd; x++) 
		{
			for(int y = yBlockLocation; y <= yBlockLocationEnd; ++y)
			{
				for(int z = zBlockLocation; z <= zBlockLocationEnd; ++z)
				{
					if(x == xBlockLocation || x == xBlockLocationEnd 
		            		|| y == yBlockLocation || y == yBlockLocationEnd
		            		|| z == zBlockLocation || z == zBlockLocationEnd)
		            		{
				                // Get the block that we are currently looping over.
				                Block currentBlock = world.getBlockAt(x, y, z);
				                // Set the block to type 57 (Diamond block!)
				                currentBlock.setType(Material.GLASS);
		            		}
				}
			}
		}
	}
}
