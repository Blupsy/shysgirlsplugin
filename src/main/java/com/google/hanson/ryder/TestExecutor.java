package com.google.hanson.ryder;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TestExecutor implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player)
		{
			Player thisPlayer = (Player) sender;
			if(Bukkit.getServer().getPlayer(thisPlayer.getUniqueId()) != null)
			{
				BanishlandCreation.CreateCubeEnclosure(thisPlayer.getLocation());
				
				return true;
			}
		}
		
		return false;
	}

}
